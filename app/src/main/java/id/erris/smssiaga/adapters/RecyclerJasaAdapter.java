package id.erris.smssiaga.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

import id.erris.smssiaga.R;
import id.erris.smssiaga.models.Jasa;
import id.erris.smssiaga.viewholders.JasaViewHolder;
import id.erris.smssiaga.viewholders.KategoriJasaViewHolder;

public class RecyclerJasaAdapter extends ExpandableRecyclerViewAdapter<KategoriJasaViewHolder, JasaViewHolder> {
    Context context;

    public RecyclerJasaAdapter(Context context, List<? extends ExpandableGroup> groups) {
        super(groups);
        this.context = context;
    }

    @Override
    public KategoriJasaViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_kategori_jasa, parent, false);
        return new KategoriJasaViewHolder(view);
    }

    @Override
    public JasaViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_jasa, parent, false);
        return new JasaViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(JasaViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        Jasa jasa = (Jasa) group.getItems().get(childIndex);
        holder.onBind(context, jasa);
    }

    @Override
    public void onBindGroupViewHolder(KategoriJasaViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.setNamaKategori(group);
    }
}
