package id.erris.smssiaga.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.google.common.base.Optional;

import java.util.List;

import id.erris.smssiaga.R;
import id.erris.smssiaga.models.Rekanan;
import id.erris.smssiaga.utils.ContactNameUtil;

public class RecyclerRekananAdapter extends RecyclerView.Adapter<RecyclerRekananAdapter.ViewHolder> {
    private Context context;
    private List<Rekanan> data;

    public RecyclerRekananAdapter(Context c, List<Rekanan> data) {
        this.context = c;
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_rekanan, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        TextDrawable drawable = TextDrawable.builder().beginConfig().width(54).height(54).endConfig()
                .buildRound(ContactNameUtil.setInitialName(data.get(position).getNamaBengkel()), R.color.colorPrimary);

        holder.imgInitial.setImageDrawable(drawable);
        holder.lblNamaBengkel.setText(data.get(position).getNamaBengkel());
        holder.lblAlamat.setText(data.get(position).getAlamat());
        holder.lblKota.setText(data.get(position).getKota());
        holder.lblNoTlp.setText(data.get(position).getNoTelepon());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgInitial;
        private TextView lblNamaBengkel, lblAlamat, lblKota, lblNoTlp;

        public ViewHolder(View itemView) {
            super(itemView);

            context = itemView.getContext();
            imgInitial = itemView.findViewById(R.id.imgRowRekananInitial);
            lblNamaBengkel = itemView.findViewById(R.id.lblRowRekananNamaBengkel);
            lblAlamat = itemView.findViewById(R.id.lblRowRekananAlamat);
            lblKota = itemView.findViewById(R.id.lblRowRekananKota);
            lblNoTlp = itemView.findViewById(R.id.lblRowRekananTelepon);
        }
    }
}
