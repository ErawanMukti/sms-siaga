package id.erris.smssiaga.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.common.base.Optional;

import java.util.List;

import id.erris.smssiaga.R;
import id.erris.smssiaga.activity.beritaDetail.BeritaDetailActivity;
import id.erris.smssiaga.models.Artikel;
import id.erris.smssiaga.retrofit.ApiClient;

public class RecyclerBeritaAdapter extends RecyclerView.Adapter<RecyclerBeritaAdapter.ViewHolder> {
    private Context context;
    private List<Artikel> data;

    public RecyclerBeritaAdapter(Context c, List<Artikel> data) {
        this.context = c;
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_berita, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Optional<String> img = Optional.fromNullable(data.get(position).getPhoto());
        if ( img.isPresent() && !img.get().equals("") ) {
            String host = ApiClient.ip_address + "preview_artikel/";
            String url_media = host + data.get(position).getPhoto().replace("artikel/", "");
            Uri uri_media = Uri.parse(url_media);

            Glide.with(this.context).load(uri_media).into(holder.imgThumb);
        }

        holder.lblJudul.setText(data.get(position).getJudul());
        holder.lblTanggal.setText(data.get(position).getCreatedAt());
        holder.lblRingkasan.setText(data.get(position).getRingkasan());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgThumb;
        private TextView lblJudul, lblTanggal, lblRingkasan;

        public ViewHolder(View itemView) {
            super(itemView);

            context = itemView.getContext();
            imgThumb = itemView.findViewById(R.id.imgRowBeritaThumb);
            lblJudul = itemView.findViewById(R.id.lblRowBeritaTitle);
            lblTanggal = itemView.findViewById(R.id.lblRowBeritaTanggal);
            lblRingkasan = itemView.findViewById(R.id.lblRowBeritaRingkasan);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = getAdapterPosition();

                    Intent intent = new Intent(context, BeritaDetailActivity.class);
                    intent.putExtra("photo", data.get(pos).getPhoto());
                    intent.putExtra("judul", data.get(pos).getJudul());
                    intent.putExtra("tanggal", data.get(pos).getCreatedAt());
                    intent.putExtra("isi", data.get(pos).getIsi());

                    context.startActivity(intent);
                }
            });
        }
    }

}

