package id.erris.smssiaga.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.view.MenuItem;

import id.erris.smssiaga.R;
import id.erris.smssiaga.activity.berita.BeritaActivity;
import id.erris.smssiaga.activity.jasa.JasaActivity;
import id.erris.smssiaga.activity.main.MainActivity;

public class BottomNavListener implements BottomNavigationView.OnNavigationItemSelectedListener {
    Context context;

    public BottomNavListener(Context context) {
        this.context = context;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intent;
        switch ( item.getItemId() ) {
            case R.id.nav_home:
                intent = new Intent(context, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

                ((Activity) context).finish();
                break;

            case R.id.nav_jasa:
                intent = new Intent(context, JasaActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

                ((Activity) context).finish();
                break;

            case R.id.nav_berita:
                intent = new Intent(context, BeritaActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

                ((Activity) context).finish();
                break;
        }
        return true;
    }
}
