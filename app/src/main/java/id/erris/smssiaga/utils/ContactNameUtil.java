package id.erris.smssiaga.utils;

import android.text.TextUtils;

public class ContactNameUtil {

    public static String setInitialName(String name) {
        if (TextUtils.isEmpty(name)) {
            name = "+";
        }

        if ( name.charAt(0) == '+') {
            return "#";
        } else {
            String[] arr_name = name.split(" ");
            String initial    = "";

            int len           = arr_name.length;
            if ( len > 2 ) { len = 2; }

            for (int i=0; i<len; i++) {
                initial += arr_name[i].charAt(0);
            }
            return initial.toUpperCase();
        }
    }
}
