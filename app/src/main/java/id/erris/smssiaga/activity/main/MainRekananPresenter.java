package id.erris.smssiaga.activity.main;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import id.erris.smssiaga.models.Rekanan;
import id.erris.smssiaga.responses.RekananResponse;
import id.erris.smssiaga.retrofit.ApiClient;
import id.erris.smssiaga.retrofit.ApiInterface;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.google.common.base.Preconditions.checkNotNull;

public class MainRekananPresenter implements MainContract.RekananPresenter {

    private final MainContract.RekananView mView;

    private List<Rekanan> rekananList;

    public MainRekananPresenter(@NonNull MainContract.RekananView view) {
        mView = checkNotNull(view, "Rekanan View Cannot Be Null!");
        mView.setPresenter(this);
    }

    @Override
    public void start() {

    }

    @Override
    public void loadRekanan(Context context) {
        rekananList = new ArrayList<>();

        ApiInterface apiInterface = ApiClient.getClient(context).create(ApiInterface.class);
        apiInterface.getRekanan()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<RekananResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d("LOAD_REKANAN", "Subscribe Start");
                    }

                    @Override
                    public void onNext(RekananResponse rekananResponse) {
                        if ( rekananResponse.getStatus().equals("ok") ) {
                            rekananList = rekananResponse.getRekanan();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("LOAD_REKANAN_ERROR", e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        mView.showRekanan(rekananList);
                    }
                });
    }

    @Override
    public void searchRekanan(Context context, String keyword) {
        mView.showDialog();
        rekananList = new ArrayList<>();

        ApiInterface apiInterface = ApiClient.getClient(context).create(ApiInterface.class);
        apiInterface.searchRekanan(keyword)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<RekananResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d("SEARCH_REKANAN", "Subscribe Start");
                    }

                    @Override
                    public void onNext(RekananResponse rekananResponse) {
                        if ( rekananResponse.getStatus().equals("ok") ) {
                            rekananList = rekananResponse.getRekanan();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("SEARCH_REKANAN_ERROR", e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        mView.showRekanan(rekananList);
                        mView.hideDialog();
                    }
                });
    }
}
