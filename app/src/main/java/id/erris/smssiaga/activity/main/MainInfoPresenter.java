package id.erris.smssiaga.activity.main;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import id.erris.smssiaga.models.Informasi;
import id.erris.smssiaga.responses.BannerResponse;
import id.erris.smssiaga.responses.InformasiResponse;
import id.erris.smssiaga.retrofit.ApiClient;
import id.erris.smssiaga.retrofit.ApiInterface;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.google.common.base.Preconditions.checkNotNull;

public class MainInfoPresenter implements MainContract.InfoPresenter {

    private final MainContract.InfoView mView;
    private Informasi informasi;
    private String banner;

    public MainInfoPresenter(@NonNull MainContract.InfoView view) {
        mView = checkNotNull(view, "Info View Cannot Be Null!");
        mView.setPresenter(this);
    }

    @Override
    public void start() {

    }

    @Override
    public void loadBanner(Context context) {
        ApiInterface apiInterface = ApiClient.getClient(context).create(ApiInterface.class);
        apiInterface.getHomeBanner()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<BannerResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d("LOAD_INFORMASI", "Subscribe Start");
                    }

                    @Override
                    public void onNext(BannerResponse bannerResponse) {
                        if ( bannerResponse.getStatus().equals("ok") ) {
                            banner = bannerResponse.getBanner();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("LOAD_INFORMASI_ERROR", e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        mView.showBanner(banner);
                        mView.hideDialog();
                    }
                });
    }

    @Override
    public void loadInformasi(Context context) {
        informasi = new Informasi();

        ApiInterface apiInterface = ApiClient.getClient(context).create(ApiInterface.class);
        apiInterface.getInformasi()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<InformasiResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d("LOAD_INFORMASI", "Subscribe Start");
                    }

                    @Override
                    public void onNext(InformasiResponse informasiResponse) {
                        if ( informasiResponse.getStatus().equals("ok") ) {
                            informasi = informasiResponse.getInformasi();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("LOAD_INFORMASI_ERROR", e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        mView.showInformasi(informasi);
                        mView.hideDialog();
                    }
                });
    }
}
