package id.erris.smssiaga.activity.berita;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.common.base.Optional;

import java.util.List;

import id.erris.smssiaga.R;
import id.erris.smssiaga.adapters.RecyclerBeritaAdapter;
import id.erris.smssiaga.models.Artikel;
import id.erris.smssiaga.retrofit.ApiClient;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A simple {@link Fragment} subclass.
 */
public class BeritaFragment extends Fragment implements BeritaContract.View{

    private BeritaContract.Presenter mPresenter;
    private Dialog loadingDialog;

    private ImageView imgBanner;
    private RecyclerView recyclerView;
    private RecyclerBeritaAdapter adapter;

    public BeritaFragment() {
        // Required empty public constructor
    }

    public static BeritaFragment newInstance() {
        return new BeritaFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void setPresenter(BeritaContract.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_berita, container, false);

        initializeResources(view);
        initializeRecycler();

        showDialog();
        mPresenter.loadBanner(getContext());
        mPresenter.loadArtikel(getContext());

        return view;
    }

    private void initializeRecycler() {
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
    }

    private void initializeResources(View view) {
        imgBanner = view.findViewById(R.id.imgBeritaBanner);
        recyclerView = view.findViewById(R.id.rcvBeritaContent);
    }

    @Override
    public void hideDialog() {
        loadingDialog.dismiss();
    }

    @Override
    public void showBanner(String banner) {
        Optional<String> img = Optional.fromNullable(banner);
        if ( img.isPresent() && !img.get().equals("") ) {
            String host = ApiClient.ip_address + "preview_banner/";
            String url_media = host + banner.replace("banner/", "");
            Uri uri_media = Uri.parse(url_media);

            Glide.with(getContext()).load(uri_media).into(imgBanner);
        }
    }

    @Override
    public void showDialog() {
        loadingDialog = ProgressDialog.show(getActivity(), "Harap Tunggu", "Mencari Data...");
        loadingDialog.setCanceledOnTouchOutside(true);
    }

    @Override
    public void showArtikel(List<Artikel> artikels) {
        adapter = new RecyclerBeritaAdapter(getContext(), artikels);
        recyclerView.setAdapter(adapter);
    }
}
