package id.erris.smssiaga.activity.jasa;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import id.erris.smssiaga.models.KategoriJasa;
import id.erris.smssiaga.responses.JasaResponse;
import id.erris.smssiaga.retrofit.ApiClient;
import id.erris.smssiaga.retrofit.ApiInterface;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.google.common.base.Preconditions.checkNotNull;

public class JasaPresenter implements JasaContract.Presenter {

    private final JasaContract.View mView;

    private List<KategoriJasa> tmpList;
    private List<KategoriJasa> kategoriJasaList;

    public JasaPresenter(@NonNull JasaContract.View view) {
        mView = checkNotNull(view, "Jasa View Cannot Be Null!");
        mView.setPresenter(this);
    }

    @Override
    public void start() {

    }

    @Override
    public void loadJasa(Context context) {
        tmpList = new ArrayList<>();
        kategoriJasaList = new ArrayList<>();

        ApiInterface apiInterface = ApiClient.getClient(context).create(ApiInterface.class);
        apiInterface.getJasa()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<JasaResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d("LOAD_JASA", "Subscribe Start");
                    }

                    @Override
                    public void onNext(JasaResponse jasaResponse) {
                        if ( jasaResponse.getStatus().equals("ok") ) {
                            tmpList = jasaResponse.getKategoriJasa();

                            for (KategoriJasa tmp2 : tmpList) {
                                KategoriJasa kategoriJasa = new KategoriJasa(tmp2.getNama(),tmp2.getJasa());
                                kategoriJasaList.add(kategoriJasa);
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("LOAD_JASA_ERROR", e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        mView.showJasa(kategoriJasaList);
                        mView.hideDialog();
                    }
                });
    }
}
