package id.erris.smssiaga.activity.jasa;

import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import id.erris.smssiaga.R;
import id.erris.smssiaga.utils.ActivityUtils;
import id.erris.smssiaga.utils.BottomNavListener;

public class JasaActivity extends AppCompatActivity {

    private JasaFragment mJasaFragment;
    private JasaPresenter mJasaPresenter;

    private BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jasa);

        initializeResources();

        setupFragment();
        setupPresenter();
    }

    private void initializeResources() {
        bottomNavigationView = findViewById(R.id.navJasaBottom);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavListener(JasaActivity.this));
    }

    private void setupPresenter() {
        mJasaPresenter = new JasaPresenter(mJasaFragment);
    }

    private void setupFragment() {
        mJasaFragment = (JasaFragment) getSupportFragmentManager().findFragmentById(R.id.frmJasaContent);
        if (mJasaFragment == null) {
            mJasaFragment = JasaFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), mJasaFragment, R.id.frmJasaContent);
        }
    }
}
