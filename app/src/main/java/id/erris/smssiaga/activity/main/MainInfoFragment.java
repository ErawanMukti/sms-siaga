package id.erris.smssiaga.activity.main;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.common.base.Optional;

import id.erris.smssiaga.R;
import id.erris.smssiaga.models.Informasi;
import id.erris.smssiaga.retrofit.ApiClient;

import static com.google.common.base.Preconditions.checkNotNull;

public class MainInfoFragment extends Fragment implements MainContract.InfoView {

    private MainContract.InfoPresenter mPresenter;

    private TextView lblJamOp, lblAlamat, lblInfoAplikasi;

    private Dialog loadingDialog;

    public MainInfoFragment() {
        // Required empty public constructor
    }

    public static MainInfoFragment newInstance() {
        return new MainInfoFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void setPresenter(MainContract.InfoPresenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_info, container, false);

        initializeResources(view);

        showDialog();
        mPresenter.loadBanner(getContext());
        mPresenter.loadInformasi(getContext());

        return view;
    }

    private void initializeResources(View view) {
        lblJamOp = view.findViewById(R.id.lblMainInfoJamOperasional);
        lblAlamat = view.findViewById(R.id.lblMainInfoAlamat);
        lblInfoAplikasi = view.findViewById(R.id.lblMainInfoAplikasi);
    }

    @Override
    public void hideDialog() {
        loadingDialog.dismiss();
    }

    @Override
    public void showBanner(String banner) {
        Optional<String> img = Optional.fromNullable(banner);
        if ( img.isPresent() && !img.get().equals("") ) {
            String host = ApiClient.ip_address + "preview_banner/";
            String url_media = host + banner.replace("banner/", "");
            Uri uri_media = Uri.parse(url_media);

            Glide.with(getContext()).load(uri_media).into(((MainActivity) getContext()).imgBanner);
        }
    }

    @Override
    public void showDialog() {
        loadingDialog = ProgressDialog.show(getActivity(), "Harap Tunggu", "Mencari Data...");
        loadingDialog.setCanceledOnTouchOutside(true);
    }

    @Override
    public void showInformasi(Informasi informasi) {
        lblJamOp.setText(informasi.getJamOperasional());
        lblAlamat.setText(informasi.getAlamat());
        lblInfoAplikasi.setText(informasi.getInfoAplikasi());
        ((MainActivity) getContext()).no_tlp = informasi.getNoTelepon();
        ((MainActivity) getContext()).lat = informasi.getLat();
        ((MainActivity) getContext()).lng = informasi.getLng();
    }
}
