package id.erris.smssiaga.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import id.erris.smssiaga.R;
import id.erris.smssiaga.activity.main.MainActivity;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        Handler handler = new Handler();
        handler.postDelayed(splash, 2500);
    }

    Runnable splash = new Runnable() {
        @Override
        public void run() {
            startActivity(new Intent(getBaseContext(), MainActivity.class));
            finish();

            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
    };
}
