package id.erris.smssiaga.activity.berita;

import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.ImageView;

import id.erris.smssiaga.R;
import id.erris.smssiaga.utils.ActivityUtils;
import id.erris.smssiaga.utils.BottomNavListener;

public class BeritaActivity extends AppCompatActivity {

    private BeritaFragment mBeritaFragment;
    private BeritaPresenter mBeritaPresenter;
    private BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_berita);

        initializeResources();

        setupFragment();
        setupPresenter();
    }

    private void initializeResources() {
        bottomNavigationView = findViewById(R.id.navBeritaBottom);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavListener(BeritaActivity.this));
    }

    private void setupPresenter() {
        mBeritaPresenter = new BeritaPresenter(mBeritaFragment);
    }

    private void setupFragment() {
        mBeritaFragment = (BeritaFragment) getSupportFragmentManager().findFragmentById(R.id.frmBeritaContent);
        if (mBeritaFragment == null) {
            // Create the fragment
            mBeritaFragment = BeritaFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), mBeritaFragment, R.id.frmBeritaContent);
        }
    }

}
