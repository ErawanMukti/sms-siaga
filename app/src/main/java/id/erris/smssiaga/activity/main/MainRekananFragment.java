package id.erris.smssiaga.activity.main;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import id.erris.smssiaga.R;
import id.erris.smssiaga.adapters.RecyclerRekananAdapter;
import id.erris.smssiaga.models.Rekanan;

import static com.google.common.base.Preconditions.checkNotNull;

public class MainRekananFragment extends Fragment implements MainContract.RekananView {

    private MainContract.RekananPresenter mPresenter;

    private EditText txtCari;
    private RecyclerView recyclerView;
    private RecyclerRekananAdapter adapter;

    private Dialog loadingDialog;

    public MainRekananFragment() {
        // Required empty public constructor
    }

    public static MainRekananFragment newInstance() {
        return new MainRekananFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void setPresenter(MainContract.RekananPresenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Override
    public void showError(String message) {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_rekanan, container, false);

        initializeResources(view);
        initializeRecycler();
        mPresenter.loadRekanan(getContext());

        return view;
    }

    private void initializeRecycler() {
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
    }

    private void initializeResources(View view) {
        txtCari = view.findViewById(R.id.txtMainRekananCari);
        recyclerView = view.findViewById(R.id.rcvMainRekanan);
        txtCari.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if ( keyEvent.getAction() == KeyEvent.ACTION_DOWN &&
                        keyCode == KeyEvent.KEYCODE_ENTER ) {
                    String keyword = "%";
                    if ( !txtCari.getText().toString().equals("") ) {
                        keyword = txtCari.getText().toString();
                    }
                    mPresenter.searchRekanan(getContext(), keyword);
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void hideDialog() {
        loadingDialog.dismiss();
    }

    @Override
    public void showDialog() {
        loadingDialog = ProgressDialog.show(getActivity(), "Harap Tunggu", "Mencari Data...");
        loadingDialog.setCanceledOnTouchOutside(true);
    }

    @Override
    public void showRekanan(List<Rekanan> rekananList) {
        adapter = new RecyclerRekananAdapter(getContext(), rekananList);
        recyclerView.setAdapter(adapter);
    }
}
