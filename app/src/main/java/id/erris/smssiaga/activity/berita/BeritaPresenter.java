package id.erris.smssiaga.activity.berita;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

import id.erris.smssiaga.models.Artikel;
import id.erris.smssiaga.responses.ArtikelResponse;
import id.erris.smssiaga.responses.BannerResponse;
import id.erris.smssiaga.retrofit.ApiClient;
import id.erris.smssiaga.retrofit.ApiInterface;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.google.common.base.Preconditions.checkNotNull;

public class BeritaPresenter implements BeritaContract.Presenter {

    private final BeritaContract.View mView;
    private String banner;
    private List<Artikel> artikels;

    public BeritaPresenter(@NonNull BeritaContract.View view) {
        mView = checkNotNull(view, "Berita View Cannot Be Null!");
        mView.setPresenter(this);
    }

    @Override
    public void loadBanner(Context context) {
        ApiInterface apiInterface = ApiClient.getClient(context).create(ApiInterface.class);
        apiInterface.getArtikelBanner()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<BannerResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d("LOAD_BERITA_BANNER", "Subscribe Start");
                    }

                    @Override
                    public void onNext(BannerResponse bannerResponse) {
                        if ( bannerResponse.getStatus().equals("ok") ) {
                            banner = bannerResponse.getBanner();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("LOAD_BANNER_ERROR", e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        mView.showBanner(banner);
                        mView.hideDialog();
                    }
                });
    }

    @Override
    public void loadArtikel(Context context) {
        ApiInterface apiInterface = ApiClient.getClient(context).create(ApiInterface.class);
        apiInterface.getArtikel()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<ArtikelResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d("LOAD_ARTIKEL_BANNER", "Subscribe Start");
                    }

                    @Override
                    public void onNext(ArtikelResponse artikelResponse) {
                        if ( artikelResponse.getStatus().equals("ok") ) {
                            artikels = artikelResponse.getData();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("LOAD_ARTIKEL_ERROR", e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        mView.showArtikel(artikels);
                        mView.hideDialog();
                    }
                });
    }

    @Override
    public void start() {

    }
}
