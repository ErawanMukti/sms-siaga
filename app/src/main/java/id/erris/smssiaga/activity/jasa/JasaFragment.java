package id.erris.smssiaga.activity.jasa;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import id.erris.smssiaga.R;
import id.erris.smssiaga.activity.main.MainContract;
import id.erris.smssiaga.adapters.RecyclerJasaAdapter;
import id.erris.smssiaga.models.KategoriJasa;

import static com.google.common.base.Preconditions.checkNotNull;

public class JasaFragment extends Fragment implements JasaContract.View {

    private JasaContract.Presenter mPresenter;

    private RecyclerView recyclerView;
    private RecyclerJasaAdapter adapter;

    private Dialog loadingDialog;

    public JasaFragment() {
        // Required empty public constructor
    }

    public static JasaFragment newInstance() {
        return new JasaFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void setPresenter(JasaContract.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_jasa, container, false);

        initializeResources(view);
        initializeRecycler();

        showDialog();
        mPresenter.loadJasa(getContext());

        return view;
    }

    private void initializeRecycler() {
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
    }

    private void initializeResources(View view) {
        recyclerView = view.findViewById(R.id.rcvJasaContent);
    }

    @Override
    public void showDialog() {
        loadingDialog = ProgressDialog.show(getActivity(), "Harap Tunggu", "Mengambil Data...");
        loadingDialog.setCanceledOnTouchOutside(true);
    }

    @Override
    public void showJasa(List<KategoriJasa> kategoriJasaList) {
        adapter = new RecyclerJasaAdapter(getContext(), kategoriJasaList);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void hideDialog() {
        loadingDialog.dismiss();
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }
}
