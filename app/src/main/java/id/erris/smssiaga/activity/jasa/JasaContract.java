package id.erris.smssiaga.activity.jasa;

import android.content.Context;

import java.util.List;

import id.erris.smssiaga.BasePresenter;
import id.erris.smssiaga.BaseView;
import id.erris.smssiaga.models.KategoriJasa;

public class JasaContract {
    public interface View extends BaseView<Presenter> {
        void showDialog();

        void showJasa(List<KategoriJasa> kategoriJasaList);

        void hideDialog();
    }

    interface Presenter extends BasePresenter {
        void loadJasa(Context context);
    }
}
