package id.erris.smssiaga.activity.jasaDetail;

import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import id.erris.smssiaga.R;

public class JasaDetailActivity extends AppCompatActivity {

    private ImageView imgBack;
    private TextView lblNama, lblDeskripsi, lblHarga;

    private String nama, deskripsi, harga;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jasa_detail);

        initializeResources();
        initializeData();
        setData();
    }

    private void setData() {
        lblNama.setText(nama);
        lblDeskripsi.setText(deskripsi);
        lblHarga.setText(harga);
    }

    private void initializeData() {
        Bundle extras = getIntent().getExtras();

        nama = extras.getString("nama");
        deskripsi = extras.getString("deskripsi");
        harga = extras.getString("harga");
    }

    private void initializeResources() {
        imgBack = findViewById(R.id.imgJasaDetailBack);
        lblNama = findViewById(R.id.lblJasaDetailNama);
        lblDeskripsi = findViewById(R.id.lblJasaDetailDeskripsi);
        lblHarga = findViewById(R.id.lblJasaDetailHarga);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
