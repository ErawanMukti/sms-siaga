package id.erris.smssiaga.activity.beritaDetail;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.common.base.Optional;

import id.erris.smssiaga.R;
import id.erris.smssiaga.retrofit.ApiClient;

public class BeritaDetailActivity extends AppCompatActivity {

    private ImageView imgThumb;
    private TextView lblJudul, lblTanggal, lblIsi;

    private String photo, judul, tanggal, isi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_berita_detail);

        initializeResources();
        initializeData();
        setData();
    }

    private void setData() {
        Optional<String> img = Optional.fromNullable(photo);
        if ( img.isPresent() && !img.get().equals("") ) {
            String host = ApiClient.ip_address + "preview_artikel/";
            String url_media = host + photo.replace("artikel/", "");
            Uri uri_media = Uri.parse(url_media);

            Glide.with(getBaseContext()).load(uri_media).into(imgThumb);
        }
        lblJudul.setText(judul);
        lblTanggal.setText(tanggal);
        lblIsi.setText(isi);
    }

    private void initializeData() {
        Bundle extras = getIntent().getExtras();

        photo = extras.getString("photo");
        judul = extras.getString("judul");
        tanggal = extras.getString("tanggal");
        isi = extras.getString("isi");
    }

    private void initializeResources() {
        imgThumb = findViewById(R.id.imgBeritaDetailThumb);
        lblJudul = findViewById(R.id.lblBeritaDetailJudul);
        lblTanggal = findViewById(R.id.lblBeritaDetailTanggal);
        lblIsi = findViewById(R.id.lblBeritaDetailisi);
    }
}
