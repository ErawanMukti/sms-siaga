package id.erris.smssiaga.activity.main;

import android.content.Context;

import java.util.List;

import id.erris.smssiaga.BasePresenter;
import id.erris.smssiaga.BaseView;
import id.erris.smssiaga.models.Informasi;
import id.erris.smssiaga.models.Rekanan;

public class MainContract {
    public interface InfoView extends BaseView<InfoPresenter> {
        void hideDialog();

        void showBanner(String banner);

        void showDialog();

        void showInformasi(Informasi informasi);
    }

    public interface RekananView extends BaseView<RekananPresenter> {
        void hideDialog();

        void showDialog();

        void showRekanan(List<Rekanan> rekananList);
    }

    interface InfoPresenter extends BasePresenter {
        void loadBanner(Context context);

        void loadInformasi(Context context);
    }

    interface RekananPresenter extends BasePresenter {
        void loadRekanan(Context context);

        void searchRekanan(Context context, String keyword);
    }
}
