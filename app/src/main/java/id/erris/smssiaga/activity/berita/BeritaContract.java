package id.erris.smssiaga.activity.berita;

import android.content.Context;

import java.util.List;

import id.erris.smssiaga.BasePresenter;
import id.erris.smssiaga.BaseView;
import id.erris.smssiaga.models.Artikel;

public class BeritaContract {
    public interface View extends BaseView<Presenter> {
        void hideDialog();

        void showBanner(String banner);

        void showDialog();

        void showArtikel(List<Artikel> artikels);
    }

    interface Presenter extends BasePresenter {
        void loadBanner(Context context);

        void loadArtikel(Context context);
    }
}
