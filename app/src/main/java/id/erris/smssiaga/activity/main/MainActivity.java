package id.erris.smssiaga.activity.main;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import id.erris.smssiaga.R;
import id.erris.smssiaga.utils.ActivityUtils;
import id.erris.smssiaga.utils.BottomNavListener;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int MY_PERMISSIONS_REQUEST_CALL = 266;

    private MainInfoFragment mInfoFragment;
    private MainInfoPresenter mInfoPresenter;

    private MainRekananFragment mRekananFragment;
    private MainRekananPresenter mRekananPresenter;

    private FrameLayout frmInfo, frmRekanan;
    private ImageView imgInfo, imgRekanan, imgTelepon, imgLokasi;
    private BottomNavigationView bottomNavigationView;

    public String no_tlp, lat, lng;
    public ImageView imgBanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeResources();

        setupFragment();
        setupPresenter();
    }

    private void initializeResources() {
        frmInfo = findViewById(R.id.frmMainInfo);
        frmRekanan = findViewById(R.id.frmMainRekanan);

        imgBanner = findViewById(R.id.imgMainBanner);
        imgInfo = findViewById(R.id.imgMainMenuInfo);
        imgRekanan = findViewById(R.id.imgMainMenuRekanan);
        imgTelepon = findViewById(R.id.imgMainMenuTelepon);
        imgLokasi = findViewById(R.id.imgMainMenuLokasi);

        bottomNavigationView = findViewById(R.id.navMainBottom);

        imgInfo.setOnClickListener(this);
        imgRekanan.setOnClickListener(this);
        imgTelepon.setOnClickListener(this);
        imgLokasi.setOnClickListener(this);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavListener(MainActivity.this));
    }

    private void setupPresenter() {
        mInfoPresenter = new MainInfoPresenter(mInfoFragment);
        mRekananPresenter = new MainRekananPresenter(mRekananFragment);
    }

    private void setupFragment() {
        mInfoFragment = (MainInfoFragment) getSupportFragmentManager().findFragmentById(R.id.frmMainInfo);
        if (mInfoFragment == null) {
            // Create the fragment
            mInfoFragment = MainInfoFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), mInfoFragment, R.id.frmMainInfo);
        }

        mRekananFragment = (MainRekananFragment) getSupportFragmentManager().findFragmentById(R.id.frmMainRekanan);
        if (mRekananFragment == null) {
            // Create the fragment
            mRekananFragment = MainRekananFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), mRekananFragment, R.id.frmMainRekanan);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgMainMenuInfo:
                frmInfo.setVisibility(View.VISIBLE);
                frmRekanan.setVisibility(View.GONE);
                break;

            case R.id.imgMainMenuRekanan:
                frmInfo.setVisibility(View.GONE);
                frmRekanan.setVisibility(View.VISIBLE);
                break;

            case R.id.imgMainMenuTelepon:
                Intent intentCall = new Intent(Intent.ACTION_CALL);
                intentCall.setData(Uri.parse("tel:"+no_tlp));
                if (ActivityCompat.checkSelfPermission(getBaseContext(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{Manifest.permission.CALL_PHONE},
                            MY_PERMISSIONS_REQUEST_CALL);
                    return;
                }
                startActivity(intentCall);
                break;

            case R.id.imgMainMenuLokasi:
                Intent intentMap = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:"+lng+","+lat+"?q="+lng+","+lat+"(Setiawan Motor Service)"));
                startActivity(intentMap);
                break;
        }
    }
}
