package id.erris.smssiaga.viewholders;

import android.view.View;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import id.erris.smssiaga.R;

public class KategoriJasaViewHolder extends GroupViewHolder {

    private TextView lblNamaKategori;

    public KategoriJasaViewHolder(View itemView) {
        super(itemView);
        lblNamaKategori = itemView.findViewById(R.id.lblRowKategoriNama);
    }

    public void setNamaKategori(ExpandableGroup group) {
        lblNamaKategori.setText(group.getTitle());
    }
}
