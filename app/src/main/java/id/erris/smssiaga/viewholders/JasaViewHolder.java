package id.erris.smssiaga.viewholders;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import id.erris.smssiaga.R;
import id.erris.smssiaga.activity.jasaDetail.JasaDetailActivity;
import id.erris.smssiaga.models.Jasa;

public class JasaViewHolder extends ChildViewHolder {

    private TextView lblNamaJasa;

    public JasaViewHolder(View itemView) {
        super(itemView);
        lblNamaJasa = itemView.findViewById(R.id.lblRowJasaNama);
    }

    public void onBind(final Context context, final Jasa jasa) {
        lblNamaJasa.setText(jasa.getNama());

        lblNamaJasa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, JasaDetailActivity.class);
                intent.putExtra("nama", jasa.getNama());
                intent.putExtra("deskripsi", jasa.getDeskripsi());
                intent.putExtra("harga", jasa.getHarga());
                context.startActivity(intent);
            }
        });
    }
}
