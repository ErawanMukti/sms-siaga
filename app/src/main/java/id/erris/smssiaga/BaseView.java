package id.erris.smssiaga;

public interface BaseView<T> {
    void setPresenter(T presenter);

    void showError(String message);
}
