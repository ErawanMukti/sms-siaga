package id.erris.smssiaga.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Informasi {
    @SerializedName("jam-operasional")
    @Expose
    private String jamOperasional;
    @SerializedName("alamat")
    @Expose
    private String alamat;
    @SerializedName("no-telepon")
    @Expose
    private String noTelepon;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lng")
    @Expose
    private String lng;
    @SerializedName("info-aplikasi")
    @Expose
    private String infoAplikasi;

    public String getJamOperasional() {
        return jamOperasional;
    }

    public void setJamOperasional(String jamOperasional) {
        this.jamOperasional = jamOperasional;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNoTelepon() {
        return noTelepon;
    }

    public void setNoTelepon(String noTelepon) {
        this.noTelepon = noTelepon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getInfoAplikasi() {
        return infoAplikasi;
    }

    public void setInfoAplikasi(String infoAplikasi) {
        this.infoAplikasi = infoAplikasi;
    }
}
