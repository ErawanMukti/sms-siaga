package id.erris.smssiaga.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class KategoriJasa extends ExpandableGroup<Jasa> {
    @SerializedName("kode")
    @Expose
    private String kode;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("jasa")
    @Expose
    private List<Jasa> jasa = null;

    public KategoriJasa(String title, List<Jasa> items) {
        super(title, items);
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public List<Jasa> getJasa() {
        return jasa;
    }

    public void setJasa(List<Jasa> jasa) {
        this.jasa = jasa;
    }
}
