package id.erris.smssiaga.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.erris.smssiaga.models.Rekanan;

public class RekananResponse {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private List<Rekanan> rekananList = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Rekanan> getRekanan() {
        return rekananList;
    }

    public void setData(List<Rekanan> rekananList) {
        this.rekananList = rekananList;
    }
}
