package id.erris.smssiaga.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.erris.smssiaga.models.Artikel;

public class ArtikelResponse {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private List<Artikel> data = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Artikel> getData() {
        return data;
    }

    public void setData(List<Artikel> data) {
        this.data = data;
    }
}
