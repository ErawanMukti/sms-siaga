package id.erris.smssiaga.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.erris.smssiaga.models.KategoriJasa;

public class JasaResponse {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private List<KategoriJasa> kategoriJasaList = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<KategoriJasa> getKategoriJasa() {
        return kategoriJasaList;
    }

    public void setData(List<KategoriJasa> kategoriJasaList) {
        this.kategoriJasaList = kategoriJasaList;
    }
}
