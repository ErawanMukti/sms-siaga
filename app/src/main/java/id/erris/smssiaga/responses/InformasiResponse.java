package id.erris.smssiaga.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import id.erris.smssiaga.models.Informasi;

public class InformasiResponse {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private Informasi informasi;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Informasi getInformasi() {
        return informasi;
    }

    public void setData(Informasi informasi) {
        this.informasi= informasi;
    }
}
