package id.erris.smssiaga.retrofit;

import id.erris.smssiaga.responses.ArtikelResponse;
import id.erris.smssiaga.responses.BannerResponse;
import id.erris.smssiaga.responses.InformasiResponse;
import id.erris.smssiaga.responses.JasaResponse;
import id.erris.smssiaga.responses.RekananResponse;
import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiInterface {
    @GET("artikels")
    Observable<ArtikelResponse> getArtikel();

    @GET("artikel_banner")
    Observable<BannerResponse> getArtikelBanner();

    @GET("home_banner")
    Observable<BannerResponse> getHomeBanner();

    @GET("informasis")
    Observable<InformasiResponse> getInformasi();

    @GET("jasas")
    Observable<JasaResponse> getJasa();

    @GET("rekanans")
    Observable<RekananResponse> getRekanan();


    @GET("rekanan/{q}")
    Observable<RekananResponse> searchRekanan(
            @Path("q") String keyword
    );
}
